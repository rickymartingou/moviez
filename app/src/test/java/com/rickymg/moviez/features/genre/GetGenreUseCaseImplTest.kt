package com.rickymg.moviez.features.genre

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import com.rickymg.moviez.network.response.Genre
import com.rickymg.moviez.network.response.GenreResponse
import com.rickymg.moviez.network.response.MainResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class GetGenreUseCaseImplTest {

    private lateinit var useCase: GetGenreUseCaseImpl

    @Mock
    private lateinit var genreRepository: GenreRepository

    @Before
    fun setUp() {
        Dispatchers.setMain(UnconfinedTestDispatcher())
        useCase = GetGenreUseCaseImpl(genreRepository)
    }

    @Test
    fun `invoke, should fetch and return the response`() = runTest {
        val genre = Genre(1, "Action")
        val genreResponse = GenreResponse(listOf(genre))
        val requestResponse = MainResponse(genreResponse, true)

        whenever(genreRepository.getGenre()).thenReturn(requestResponse)

        val result = useCase.invoke()

        verify(genreRepository).getGenre()

        assertEquals(genreResponse.genres.size, result?.data?.genres?.size)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        verifyNoMoreInteractions(genreRepository)
    }
}