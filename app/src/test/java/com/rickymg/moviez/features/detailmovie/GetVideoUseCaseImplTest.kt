package com.rickymg.moviez.features.detailmovie

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import com.rickymg.moviez.network.response.MainResponse
import com.rickymg.moviez.network.response.Video
import com.rickymg.moviez.network.response.VideoResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class GetVideoUseCaseImplTest {

    private lateinit var useCase: GetVideoUseCaseImpl

    @Mock
    private lateinit var movieRepository: DetailMovieRepository

    @Before
    fun setUp() {
        Dispatchers.setMain(UnconfinedTestDispatcher())
        useCase = GetVideoUseCaseImpl(movieRepository)
    }

    @Test
    fun `invoke, should fetch and return the response`() = runTest {
        val video = Video()
        val videoResponse = VideoResponse(1, listOf(video))
        val requestResponse = MainResponse(videoResponse, true)

        whenever(movieRepository.getVideo(1)).thenReturn(requestResponse)

        val result = useCase.invoke(1)

        verify(movieRepository).getVideo(1)

        assertEquals(videoResponse.results.size, result?.data?.results?.size)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        verifyNoMoreInteractions(movieRepository)
    }
}