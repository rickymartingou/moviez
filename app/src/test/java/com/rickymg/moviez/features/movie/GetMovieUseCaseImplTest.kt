package com.rickymg.moviez.features.movie

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import com.rickymg.moviez.network.response.MainResponse
import com.rickymg.moviez.network.response.Movie
import com.rickymg.moviez.network.response.MovieResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class GetMovieUseCaseImplTest {

    private lateinit var useCase: GetMovieUseCaseImpl

    @Mock
    private lateinit var movieRepository: MovieRepository

    @Before
    fun setUp() {
        Dispatchers.setMain(UnconfinedTestDispatcher())
        useCase = GetMovieUseCaseImpl(movieRepository)
    }

    @Test
    fun `invoke, should fetch and return the response`() = runTest {
        val movie = Movie()
        val movieResponse = MovieResponse(1, listOf(movie))
        val requestResponse = MainResponse(movieResponse, true)

        whenever(movieRepository.getMovie(1, 123)).thenReturn(requestResponse)

        val result = useCase.invoke(1, 123)

        verify(movieRepository).getMovie(1, 123)

        assertEquals(movieResponse.results.size, result?.data?.results?.size)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        verifyNoMoreInteractions(movieRepository)
    }
}