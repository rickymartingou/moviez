package com.rickymg.moviez

import com.rickymg.moviez.features.genre.GetGenreModule
import com.rickymg.moviez.features.movie.GetMovieModule
import com.rickymg.moviez.features.detailmovie.DetailMovieModule
import com.rickymg.moviez.network.NetworkModule
import com.rickymg.moviez.ui.detail.DetailMovieFragment
import com.rickymg.moviez.ui.genre.GenreFragment
import com.rickymg.moviez.ui.movie.MovieFragment
import dagger.Component

@Component(
    modules = [
        AppModule::class,
        NetworkModule::class,

        // Features
        GetGenreModule::class,
        GetMovieModule::class,
        DetailMovieModule::class,
    ]
)
interface AppComponent {
    fun inject(activity: MainActivity)
    fun inject(fragment: GenreFragment)
    fun inject(fragment: MovieFragment)
    fun inject(fragment: DetailMovieFragment)
}