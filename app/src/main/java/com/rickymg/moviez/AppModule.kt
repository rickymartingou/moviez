package com.rickymg.moviez

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

@Module
internal class AppModule(private val context: Application) {
    @Provides
    fun provideContext(): Context = context
}