package com.rickymg.moviez.ui.genre

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.GridLayoutManager
import com.rickymg.moviez.App
import com.rickymg.moviez.databinding.FragmentGenreBinding
import javax.inject.Inject

class GenreFragment : Fragment() {

    private lateinit var binding: FragmentGenreBinding
    private lateinit var viewModel: GenreViewModel
    private lateinit var adapter: GenreAdapter

    @Inject
    lateinit var viewModelFactory: GenreViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpDependency()
        initViewModel()
        fetchData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGenreBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(
            this,
            viewModelFactory
        )[GenreViewModel::class.java]
    }

    private fun fetchData() {
        viewModel.getGenre()
    }

    private fun setUpDependency() {
        (activity?.application as App).appComponent.inject(this)
    }

    private fun initView() {
        adapter = GenreAdapter()
        adapter.onItemClicked = {
            val action = GenreFragmentDirections.redirectToMovie(it)
            NavHostFragment.findNavController(this).navigate(action)
        }
        binding.rvGenre.adapter = adapter
        binding.rvGenre.layoutManager = GridLayoutManager(binding.root.context, 2)

        viewModel.getGenreResult.observe(viewLifecycleOwner) {
            if (it?.isSuccess == true) {
                binding.rvGenre.isVisible = true
                binding.progressbar.isVisible = false

                adapter.setData(it.data?.genres!!)
                adapter.notifyDataSetChanged()
            }
        }
    }
}