package com.rickymg.moviez.ui.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rickymg.moviez.features.detailmovie.GetReviewUseCase
import com.rickymg.moviez.features.detailmovie.GetVideoUseCase

class DetailMovieViewModelFactory(
    private val getReviewUseCase: GetReviewUseCase,
    private val getVideoUseCase: GetVideoUseCase,
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return DetailMovieViewModel(getReviewUseCase, getVideoUseCase) as T
    }
}