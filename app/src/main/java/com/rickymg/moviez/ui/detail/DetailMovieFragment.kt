package com.rickymg.moviez.ui.detail

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.rickymg.moviez.App
import com.rickymg.moviez.BuildConfig
import com.rickymg.moviez.databinding.FragmentDetailMovieBinding
import com.rickymg.moviez.network.response.Movie
import com.rickymg.moviez.network.response.Review
import com.rickymg.moviez.ui.detail.DetailMovieFragment.Constants.LIMIT
import com.rickymg.moviez.ui.detail.DetailMovieFragment.Constants.PAGE_START
import com.rickymg.moviez.ui.movie.MovieFragment
import javax.inject.Inject

class DetailMovieFragment : Fragment() {

    object Constants {
        const val PAGE_START = 1
        const val LIMIT = 20
    }

    private lateinit var binding: FragmentDetailMovieBinding
    private lateinit var movie: Movie
    private lateinit var adapter: ReviewAdapter
    private lateinit var viewModel: DetailMovieViewModel

    private var currentPage = PAGE_START
    private var isLastPage = false
    private var isLoadMore = false

    private val reviewData = arrayListOf<Review>()
    private var videoKey = ""

    @Inject
    lateinit var viewModelFactory: DetailMovieViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            movie = DetailMovieFragmentArgs.fromBundle(requireArguments()).movie!!
        }
        setUpDependency()
        initViewModel()
        fetchData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailMovieBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        binding.toolbar.tvTitle.text = "Detail Movie"
        binding.toolbar.toolbar.setNavigationOnClickListener {
            NavHostFragment.findNavController(this).navigateUp()
        }

        binding.ivBanner.setImageURI(BuildConfig.BASE_IMAGE_URL + "w500" + movie.backdropPath)
        binding.tvMTitle.text = movie.title
        binding.tvReleaseDate.text = movie.releaseDate
        binding.tvOverview.text = movie.overview
        binding.btnPlay.setOnClickListener {
            openYoutube()
        }

        binding.layoutNested.setOnScrollChangeListener(
            NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
                if (scrollY >= (v.getChildAt(0).measuredHeight - v.measuredHeight)) {
                    if (!isLastPage && isLoadMore) {
                        isLoadMore = !isLoadMore
                        currentPage++
                        viewModel.getReview(movie.id!!, currentPage)
                    }
                }
            }
        )

        adapter = ReviewAdapter()
        binding.rvReview.adapter = adapter
        binding.rvReview.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        viewModel.getVideoResult.observe(viewLifecycleOwner) {
            if (it?.isSuccess == true) {
                if (it.data?.results?.isNotEmpty()!!) {
                    videoKey = it.data.results[0].key!!
                    binding.btnPlay.isVisible = true
                }
            }
        }

        viewModel.getReviewResult.observe(viewLifecycleOwner) {
            if (it?.isSuccess == true) {
                binding.progressbar.isVisible = false
                isLastPage =
                    (it.data?.results?.size!! < LIMIT) || it.data.results.isEmpty()
                isLoadMore = !isLastPage
                if (currentPage == MovieFragment.Constants.PAGE_START) {
                    reviewData.clear()
                }
                if (it.data.results.isEmpty()) {
                    binding.rvReview.isVisible = false
                    binding.tvNoReview.isVisible = true
                } else {
                    binding.rvReview.isVisible = true
                    binding.tvNoReview.isVisible = false
                    reviewData.addAll(it.data.results)
                    adapter.setData(reviewData)
                    adapter.notifyDataSetChanged()
                }
            }
        }
    }

    private fun setUpDependency() {
        (activity?.application as App).appComponent.inject(this)
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(
            this,
            viewModelFactory
        )[DetailMovieViewModel::class.java]
    }

    private fun fetchData() {
        viewModel.getReview(movie.id!!, 1)
        viewModel.getVideo(movie.id!!)
    }

    private fun openYoutube() {
        val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$videoKey"))
        val webIntent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse("http://www.youtube.com/watch?v=$id")
        )
        try {
            binding.root.context.startActivity(appIntent)
        } catch (ex: ActivityNotFoundException) {
            binding.root.context.startActivity(webIntent)
        }
    }
}