package com.rickymg.moviez.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rickymg.moviez.features.detailmovie.GetReviewUseCase
import com.rickymg.moviez.features.detailmovie.GetVideoUseCase
import com.rickymg.moviez.network.response.MainResponse
import com.rickymg.moviez.network.response.ReviewResponse
import com.rickymg.moviez.network.response.VideoResponse
import kotlinx.coroutines.launch

class DetailMovieViewModel(
    private val getReviewUseCase: GetReviewUseCase,
    private val getVideoUseCase: GetVideoUseCase,
) : ViewModel() {

    private val _getReviewResult = MutableLiveData<MainResponse<ReviewResponse>?>()
    val getReviewResult: LiveData<MainResponse<ReviewResponse>?> = _getReviewResult

    private val _getVideoResult = MutableLiveData<MainResponse<VideoResponse>?>()
    val getVideoResult: LiveData<MainResponse<VideoResponse>?> = _getVideoResult

    fun getReview(id: Int, page: Int) {
        viewModelScope.launch {
            val response = getReviewUseCase.invoke(id, page)
            _getReviewResult.postValue(response)
        }
    }

    fun getVideo(id: Int) {
        viewModelScope.launch {
            val response = getVideoUseCase.invoke(id)
            _getVideoResult.postValue(response)
        }
    }
}