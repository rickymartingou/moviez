package com.rickymg.moviez.ui.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rickymg.moviez.BuildConfig
import com.rickymg.moviez.databinding.ItemReviewBinding
import com.rickymg.moviez.network.response.Review

class ReviewAdapter : RecyclerView.Adapter<ReviewAdapter.ViewHolder>() {

    private var reviews = mutableListOf<Review>()

    fun setData(data: List<Review>) {
        this.reviews.clear()
        this.reviews.addAll(data)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemReviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(reviews[position])
    }

    override fun getItemCount(): Int = reviews.size

    inner class ViewHolder(
        private val binding: ItemReviewBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bindItem(review: Review) {
            binding.ivProfilePicture.setImageURI(BuildConfig.BASE_IMAGE_URL + "w185" + review.authorDetails?.avatarPath)
            binding.tvAuthor.text = review.author
            binding.tvReview.text = review.content
        }
    }
}