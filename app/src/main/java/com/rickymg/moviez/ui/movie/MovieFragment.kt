package com.rickymg.moviez.ui.movie

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.rickymg.moviez.App
import com.rickymg.moviez.databinding.FragmentMovieBinding
import com.rickymg.moviez.network.response.Genre
import com.rickymg.moviez.network.response.Movie
import com.rickymg.moviez.ui.movie.MovieFragment.Constants.LIMIT
import com.rickymg.moviez.ui.movie.MovieFragment.Constants.PAGE_START
import javax.inject.Inject

class MovieFragment : Fragment() {

    object Constants {
        const val PAGE_START = 1
        const val LIMIT = 20
    }

    private lateinit var binding: FragmentMovieBinding
    private lateinit var genre: Genre
    private lateinit var adapter: MovieAdapter
    private lateinit var viewModel: MovieViewModel

    private var currentPage = PAGE_START
    private var isLastPage = false
    private var isLoadMore = false

    private val movieData = arrayListOf<Movie>()
    private val lastMovieData = arrayListOf<Movie>()

    @Inject
    lateinit var viewModelFactory: MovieViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            genre = MovieFragmentArgs.fromBundle(requireArguments()).genre!!
        }
        setUpDependency()
        initViewModel()
        fetchData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMovieBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        binding.toolbar.tvTitle.text = genre.name
        binding.toolbar.toolbar.setNavigationOnClickListener {
            NavHostFragment.findNavController(this).navigateUp()
        }

        binding.layoutNested.setOnScrollChangeListener(
            NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
                if (scrollY >= (v.getChildAt(0).measuredHeight - v.measuredHeight)) {
                    if (!isLastPage && isLoadMore) {
                        isLoadMore = !isLoadMore
                        currentPage++
                        viewModel.getMovie(currentPage, genre.id)
                    }
                }
            }
        )

        adapter = MovieAdapter()
        adapter.onItemClicked = {
            val action = MovieFragmentDirections.redirectToDetailMovie(it)
            NavHostFragment.findNavController(this).navigate(action)
        }
        binding.rvMovie.adapter = adapter
        binding.rvMovie.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        viewModel.getMovieResult.observe(viewLifecycleOwner) {
            if (it?.isSuccess == true) {
                binding.rvMovie.isVisible = true
                binding.progressbar.isVisible = false

                if (lastMovieData.isEmpty()) {
                    lastMovieData.addAll(it.data?.results!!)
                } else {
                    if (lastMovieData == it.data?.results) {
                        adapter.setData(movieData)
                        return@observe
                    } else {
                        lastMovieData.clear()
                        lastMovieData.addAll(it.data?.results!!)
                    }
                }

                isLastPage = (it.data?.results?.size!! < LIMIT) || it.data.results.isEmpty()
                isLoadMore = !isLastPage
                if (currentPage == PAGE_START) {
                    movieData.clear()
                }
                movieData.addAll(it.data.results)
                adapter.setData(movieData)
                adapter.notifyDataSetChanged()
            }
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(
            this,
            viewModelFactory
        )[MovieViewModel::class.java]
    }

    private fun fetchData() {
        viewModel.getMovie(currentPage, genre.id)
    }

    private fun setUpDependency() {
        (activity?.application as App).appComponent.inject(this)
    }
}