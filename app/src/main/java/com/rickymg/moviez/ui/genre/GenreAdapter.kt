package com.rickymg.moviez.ui.genre

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rickymg.moviez.databinding.ItemGenreBinding
import com.rickymg.moviez.network.response.Genre

class GenreAdapter() :
    RecyclerView.Adapter<GenreAdapter.ViewHolder>() {
    var onItemClicked: ((Genre) -> Unit)? = null

    private var genres = mutableListOf<Genre>()

    fun setData(data: List<Genre>) {
        this.genres.clear()
        this.genres.addAll(data)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemGenreBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(onItemClicked, binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(genres[position])
    }

    override fun getItemCount(): Int = genres.size

    inner class ViewHolder(
        private val onItemClicked: ((Genre) -> Unit)?,
        private val binding: ItemGenreBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bindItem(genre: Genre) {
            binding.root.setOnClickListener {
                onItemClicked?.invoke(genre)
            }
            binding.tvTitle.text = genre.name
        }
    }
}