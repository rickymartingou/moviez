package com.rickymg.moviez.ui.genre

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rickymg.moviez.features.genre.GetGenreUseCase
import com.rickymg.moviez.network.response.GenreResponse
import com.rickymg.moviez.network.response.MainResponse
import kotlinx.coroutines.launch

class GenreViewModel(
    private val getGenreUseCase: GetGenreUseCase
) : ViewModel() {
    private val _getGenreResult = MutableLiveData<MainResponse<GenreResponse>?>()
    val getGenreResult: LiveData<MainResponse<GenreResponse>?> = _getGenreResult

    fun getGenre() {
        viewModelScope.launch {
            val response = getGenreUseCase.invoke()
            _getGenreResult.postValue(response)
        }
    }
}