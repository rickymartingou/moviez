package com.rickymg.moviez.ui.movie

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rickymg.moviez.features.genre.GetGenreUseCase
import com.rickymg.moviez.features.movie.GetMovieUseCase

class MovieViewModelFactory(
    private val getMovieUseCase: GetMovieUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MovieViewModel(getMovieUseCase) as T
    }
}