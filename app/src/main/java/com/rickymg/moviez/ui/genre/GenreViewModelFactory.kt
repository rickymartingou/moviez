package com.rickymg.moviez.ui.genre

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rickymg.moviez.features.genre.GetGenreUseCase

class GenreViewModelFactory(
    private val getGenreUseCase: GetGenreUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return GenreViewModel(getGenreUseCase) as T
    }
}