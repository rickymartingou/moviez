package com.rickymg.moviez.ui.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rickymg.moviez.BuildConfig
import com.rickymg.moviez.databinding.ItemMovieBinding
import com.rickymg.moviez.network.response.Movie

class MovieAdapter : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {
    var onItemClicked: ((Movie) -> Unit)? = null

    private var movies = mutableListOf<Movie>()

    fun setData(data: List<Movie>) {
        this.movies.clear()
        this.movies.addAll(data)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(onItemClicked, binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(movies[position])
    }

    override fun getItemCount(): Int = movies.size

    inner class ViewHolder(
        private val onItemClicked: ((Movie) -> Unit)?,
        private val binding: ItemMovieBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bindItem(movie: Movie) {
            binding.root.setOnClickListener {
                onItemClicked?.invoke(movie)
            }
            binding.tvTitle.text = movie.title
            binding.tvReleaseDate.text = movie.releaseDate
            binding.ivPoster.setImageURI(BuildConfig.BASE_IMAGE_URL + "w185" + movie.posterPath)
        }
    }
}