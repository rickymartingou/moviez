package com.rickymg.moviez.ui.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rickymg.moviez.features.movie.GetMovieUseCase
import com.rickymg.moviez.network.response.MainResponse
import com.rickymg.moviez.network.response.MovieResponse
import kotlinx.coroutines.launch

class MovieViewModel(
    private val getMovieUseCase: GetMovieUseCase
) : ViewModel() {
    private val _getMovieResult = MutableLiveData<MainResponse<MovieResponse>?>()
    val getMovieResult: LiveData<MainResponse<MovieResponse>?> = _getMovieResult

    fun getMovie(page: Int, id: Int) {
        viewModelScope.launch {
            val response = getMovieUseCase.invoke(page, id)
            _getMovieResult.postValue(response)
        }
    }
}