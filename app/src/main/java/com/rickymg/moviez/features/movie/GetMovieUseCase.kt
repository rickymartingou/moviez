package com.rickymg.moviez.features.movie

import com.rickymg.moviez.network.response.MainResponse
import com.rickymg.moviez.network.response.MovieResponse

interface GetMovieUseCase {
    suspend operator fun invoke(
        page: Int,
        id: Int,
    ): MainResponse<MovieResponse>?
}

class GetMovieUseCaseImpl(private val repository: MovieRepository) : GetMovieUseCase {
    override suspend fun invoke(
        page: Int,
        id: Int,
    ): MainResponse<MovieResponse>? {
        return repository.getMovie(page, id)
    }
}