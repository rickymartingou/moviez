package com.rickymg.moviez.features.detailmovie

import com.rickymg.moviez.network.MovieService
import com.rickymg.moviez.network.response.MainResponse
import com.rickymg.moviez.network.response.ReviewResponse
import com.rickymg.moviez.network.response.VideoResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import java.io.IOException

interface DetailMovieRepository {
    suspend fun getReview(id: Int, page: Int): MainResponse<ReviewResponse>?

    suspend fun getVideo(id: Int): MainResponse<VideoResponse>?
}

class DetailMovieRepositoryImpl(
    private val movieService: MovieService,
    private val dispatcher: CoroutineDispatcher
) : DetailMovieRepository {
    override suspend fun getReview(
        id: Int,
        page: Int,
    ): MainResponse<ReviewResponse>? {
        return withContext(dispatcher) {
            try {
                val response = movieService.getMovieReviews(id, page)
                val responseBody = response.body()
                if (response.isSuccessful && responseBody != null) {
                    MainResponse(responseBody, true)
                } else {
                    MainResponse(null, false)
                }
            } catch (ioException: IOException) {
                null
            }
        }
    }

    override suspend fun getVideo(id: Int): MainResponse<VideoResponse>? {
        return withContext(dispatcher) {
            try {
                val response = movieService.getMovieVideos(id)
                val responseBody = response.body()
                if (response.isSuccessful && responseBody != null) {
                    MainResponse(responseBody, true)
                } else {
                    MainResponse(null, false)
                }
            } catch (ioException: IOException) {
                null
            }
        }
    }
}