package com.rickymg.moviez.features.movie

import com.rickymg.moviez.network.MovieService
import com.rickymg.moviez.network.response.MainResponse
import com.rickymg.moviez.network.response.MovieResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import java.io.IOException

interface MovieRepository {
    suspend fun getMovie(
        page: Int,
        id: Int,
    ): MainResponse<MovieResponse>?
}

class MovieRepositoryImpl(
    private val movieService: MovieService,
    private val dispatcher: CoroutineDispatcher
) : MovieRepository {
    override suspend fun getMovie(
        page: Int,
        id: Int,
    ): MainResponse<MovieResponse>? {
        return withContext(dispatcher) {
            try {
                val response = movieService.getMovieByGenreId(page, id)
                val responseBody = response.body()
                if (response.isSuccessful && responseBody != null) {
                    MainResponse(responseBody, true)
                } else {
                    MainResponse(null, false)
                }
            } catch (ioException: IOException) {
                null
            }
        }
    }
}