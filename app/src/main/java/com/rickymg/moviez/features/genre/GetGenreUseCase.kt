package com.rickymg.moviez.features.genre

import com.rickymg.moviez.network.response.GenreResponse
import com.rickymg.moviez.network.response.MainResponse

interface GetGenreUseCase {
    suspend operator fun invoke(): MainResponse<GenreResponse>?
}

class GetGenreUseCaseImpl(private val repository: GenreRepository) : GetGenreUseCase {
    override suspend fun invoke(): MainResponse<GenreResponse>? {
        return repository.getGenre()
    }
}