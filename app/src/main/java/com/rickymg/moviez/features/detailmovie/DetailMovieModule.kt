package com.rickymg.moviez.features.detailmovie

import com.rickymg.moviez.network.MovieService
import com.rickymg.moviez.ui.detail.DetailMovieViewModelFactory
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Dispatchers

@Module
internal object DetailMovieModule {

    @Provides
    fun provideDetailMovieViewModelFactory(
        getReviewUseCase: GetReviewUseCase,
        getVideoUseCase: GetVideoUseCase,
    ): DetailMovieViewModelFactory {
        return DetailMovieViewModelFactory(getReviewUseCase, getVideoUseCase)
    }

    @Provides
    fun provideDetailMovieRepository(
        movieService: MovieService
    ): DetailMovieRepository {
        return DetailMovieRepositoryImpl(movieService, Dispatchers.IO)
    }

    @Provides
    fun provideGetReviewUseCase(detailMovieRepository: DetailMovieRepository): GetReviewUseCase {
        return GetReviewUseCaseImpl(detailMovieRepository)
    }

    @Provides
    fun provideGetVideoUseCase(detailMovieRepository: DetailMovieRepository): GetVideoUseCase {
        return GetVideoUseCaseImpl(detailMovieRepository)
    }
}