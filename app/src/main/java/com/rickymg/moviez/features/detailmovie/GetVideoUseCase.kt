package com.rickymg.moviez.features.detailmovie

import com.rickymg.moviez.network.response.MainResponse
import com.rickymg.moviez.network.response.VideoResponse

interface GetVideoUseCase {
    suspend operator fun invoke(id: Int): MainResponse<VideoResponse>?
}

class GetVideoUseCaseImpl(private val repository: DetailMovieRepository) : GetVideoUseCase {
    override suspend fun invoke(id: Int): MainResponse<VideoResponse>? {
        return repository.getVideo(id)
    }
}