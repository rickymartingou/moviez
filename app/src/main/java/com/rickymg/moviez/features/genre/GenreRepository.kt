package com.rickymg.moviez.features.genre

import com.rickymg.moviez.network.MovieService
import com.rickymg.moviez.network.response.GenreResponse
import com.rickymg.moviez.network.response.MainResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import java.io.IOException

interface GenreRepository {
    suspend fun getGenre(): MainResponse<GenreResponse>?
}

class GenreRepositoryImpl(
    private val movieService: MovieService,
    private val dispatcher: CoroutineDispatcher
) : GenreRepository {
    override suspend fun getGenre():MainResponse<GenreResponse>? {
        return withContext(dispatcher) {
            try {
                val response = movieService.getMovieGenres()
                val responseBody = response.body()
                if (response.isSuccessful && responseBody != null) {
                    MainResponse(responseBody, true)
                } else {
                    MainResponse(null, false)
                }
            } catch (ioException: IOException) {
                null
            }
        }
    }
}