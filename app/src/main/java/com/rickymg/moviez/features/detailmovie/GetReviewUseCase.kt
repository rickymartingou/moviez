package com.rickymg.moviez.features.detailmovie

import com.rickymg.moviez.network.response.MainResponse
import com.rickymg.moviez.network.response.ReviewResponse

interface GetReviewUseCase {
    suspend operator fun invoke(id: Int, page: Int): MainResponse<ReviewResponse>?
}

class GetReviewUseCaseImpl(private val repository: DetailMovieRepository) : GetReviewUseCase {
    override suspend fun invoke(id: Int, page: Int): MainResponse<ReviewResponse>? {
        return repository.getReview(id, page)
    }
}