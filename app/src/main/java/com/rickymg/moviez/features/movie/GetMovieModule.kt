package com.rickymg.moviez.features.movie

import com.rickymg.moviez.network.MovieService
import com.rickymg.moviez.ui.movie.MovieViewModelFactory
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Dispatchers

@Module
internal object GetMovieModule {

    @Provides
    fun provideMovieViewModelFactory(
        getMovieUseCase: GetMovieUseCase
    ): MovieViewModelFactory {
        return MovieViewModelFactory(getMovieUseCase)
    }

    @Provides
    fun provideMovieRepository(
        movieService: MovieService
    ): MovieRepository {
        return MovieRepositoryImpl(movieService, Dispatchers.IO)
    }

    @Provides
    fun provideGetMovieUseCase(movieRepository: MovieRepository): GetMovieUseCase {
        return GetMovieUseCaseImpl(movieRepository)
    }
}