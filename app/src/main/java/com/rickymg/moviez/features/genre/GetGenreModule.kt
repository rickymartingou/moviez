package com.rickymg.moviez.features.genre

import com.rickymg.moviez.network.MovieService
import com.rickymg.moviez.ui.genre.GenreViewModelFactory
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Dispatchers

@Module
internal object GetGenreModule {

    @Provides
    fun provideGenreViewModelFactory(
        getGenreUseCase: GetGenreUseCase
    ): GenreViewModelFactory {
        return GenreViewModelFactory(getGenreUseCase)
    }

    @Provides
    fun provideGenreRepository(
        movieService: MovieService
    ): GenreRepository {
        return GenreRepositoryImpl(movieService, Dispatchers.IO)
    }

    @Provides
    fun provideGetGenreUseCase(genreRepository: GenreRepository): GetGenreUseCase {
        return GetGenreUseCaseImpl(genreRepository)
    }
}