package com.rickymg.moviez.network.response

import com.google.gson.annotations.SerializedName

data class MainResponse<Any>(
    @SerializedName("data")
    val data: Any? = null,

    @SerializedName("isSuccess")
    val isSuccess: Boolean? = null,
)
