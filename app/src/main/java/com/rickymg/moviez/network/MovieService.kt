package com.rickymg.moviez.network

import com.rickymg.moviez.network.response.GenreResponse
import com.rickymg.moviez.network.response.MovieResponse
import com.rickymg.moviez.network.response.ReviewResponse
import com.rickymg.moviez.network.response.VideoResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieService {
    @GET("genre/movie/list")
    suspend fun getMovieGenres(): Response<GenreResponse>

    @GET("discover/movie")
    suspend fun getMovieByGenreId(
        @Query("page") page: Int,
        @Query("with_genres") with_genres: Int,
    ): Response<MovieResponse>

    @GET("movie/{id}/videos")
    suspend fun getMovieVideos(
        @Path("id") id: Int,
    ): Response<VideoResponse>

    @GET("movie/{id}/reviews")
    suspend fun getMovieReviews(
        @Path("id") id: Int,
        @Query("page") page: Int,
    ): Response<ReviewResponse>
}