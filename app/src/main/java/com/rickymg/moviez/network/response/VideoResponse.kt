package com.rickymg.moviez.network.response

import com.google.gson.annotations.SerializedName

data class VideoResponse(
    @SerializedName("id")
    var id: Int,
    @SerializedName("results")
    var results: List<Video>,
)

data class Video(
    @SerializedName("name")
    var name: String? = null,

    @SerializedName("key")
    var key: String? = null,

    @SerializedName("id")
    var id: String? = null
)
