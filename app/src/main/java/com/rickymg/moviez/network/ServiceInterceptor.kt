package com.rickymg.moviez.network

import com.rickymg.moviez.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class ServiceInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        val token = BuildConfig.TMDB_TOKEN

        val finalToken = "Bearer $token"
        request = request.newBuilder()
            .addHeader("Authorization", finalToken)
            .build()

        return chain.proceed(request)
    }

}