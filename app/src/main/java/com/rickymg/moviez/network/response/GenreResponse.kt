package com.rickymg.moviez.network.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class GenreResponse(
    @SerializedName("genres")
    var genres: List<Genre>,
)

@Parcelize
data class Genre(
    @SerializedName("id")
    var id: Int,
    @SerializedName("name")
    var name: String
) : Parcelable
